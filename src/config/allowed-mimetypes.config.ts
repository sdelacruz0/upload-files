


const imagesMimetypes = ["image/png", "image/gif", "image/jpeg", "image/svg+xml"];
const officesMimeTypes = ["application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", , "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel.sheet.binary.macroEnabled.12"];
const generalMimeTypes = ["application/pdf"];

export class AllowedMimeType {

    static getAll(): string[] {
        return imagesMimetypes.concat(officesMimeTypes, generalMimeTypes);
    }

    static getImagesMimeTypes(): string[] {
        return imagesMimetypes;
    }

    static getGeneralMimeTypes(): string[] {
        return officesMimeTypes.concat(generalMimeTypes);
    }
}