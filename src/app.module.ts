import { FilePathMiddleware } from '@common/middleware';
import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { FilesModule } from './modules/files/files.module';
import { UploadedModule } from './modules/uploaded/uploaded.module';

@Module({
  imports: [FilesModule, UploadedModule],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(FilePathMiddleware)
      .forRoutes({ path: 'uploaded/*', method: RequestMethod.GET });
  }
}
