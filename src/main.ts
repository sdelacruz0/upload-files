import { NestFactory } from '@nestjs/core';
import { ValidationPipe, Logger } from '@nestjs/common';
// import { HttpResponseInterceptor } from '@common/interceptors';
// import { HttpExceptionFilter } from '@common/exceptions';

import * as compression from 'compression';
import * as helmet from 'helmet';

import { AppModule } from './app.module';
import { configSwagger } from './config/swagger.config';


async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const port = 4000;

  // app.useGlobalFilters(new HttpExceptionFilter());
  // app.useGlobalInterceptors(new HttpResponseInterceptor());
  // app.useGlobalPipes(new ValidationPipe());

  app.use(helmet());
  app.use(compression());
  app.enableCors();
  configSwagger(app);
  await app.listen(port);
  return port;
}
bootstrap().then((port: number) => {
  Logger.log(`Application running on port: ${port}`, 'Main');
});
