import { DocumentPath, ImagesSize } from '.';
import { ErrorType } from '../common/enum/error-type.enum';


export type errorUploadingFiles =
    | {
        errorType: ErrorType.SavingImages
        error: any
        savedImages: ImagesSize[];
    }
    | {
        errorType: ErrorType.SavingGeneralFiles;
        error: any
        savedDocuments: DocumentPath[]
    }