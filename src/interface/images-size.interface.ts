
export interface ImagesSize {
    thumb: string;
    low: string;
    medium: string;
    original: string;
}