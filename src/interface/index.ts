import { DocumentPath } from './document-path.interface'
import { ImagesSize } from './images-size.interface'
import { FileType } from './file.type';
import { errorUploadingFiles } from './error-uploading-files.type'
export {
    DocumentPath,
    ImagesSize,
    FileType,
    errorUploadingFiles
}