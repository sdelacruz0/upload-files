import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { ExtractJwt } from 'passport-jwt';
import { TokenUploadFileService } from '../services';



@Injectable()
export class TokenUploadFileGuard implements CanActivate {
    constructor(
        private tokenUploadFileService: TokenUploadFileService
    ) { }

    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        const token = ExtractJwt.fromAuthHeaderAsBearerToken()(
            context.switchToHttp().getRequest(),
        )

        try {
            this.tokenUploadFileService.verifyUploadToken(token)
            return true;
        } catch (error) {
            return false;
        }
    }
}
