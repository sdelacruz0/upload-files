import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { FilesController } from './files.controller';
import { diskStorage } from 'multer';
// import { ServeStaticModule } from '@nestjs/serve-static';
// import { join } from 'path'
import { fileHelper } from '@helpers';
import { FilesService, TokenUploadFileService } from './services';


@Module({
  imports: [
    // ServeStaticModule.forRoot({
    //   rootPath: join(__dirname, '../../../..', 'storage'),
    //   serveRoot: '/storage'
    // }),
    MulterModule.register({
      fileFilter: fileHelper.validateFilesMimeType,
      storage: diskStorage({
        destination: '../temp',
        filename: fileHelper.editFileName
      }),
    })
  ],
  controllers: [FilesController],
  providers: [FilesService, TokenUploadFileService]
})
export class FilesModule { }
