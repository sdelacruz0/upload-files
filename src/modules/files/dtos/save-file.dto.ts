import { ApiProperty } from "@nestjs/swagger";
import { IsOptional, MinLength } from "class-validator";


export class SaveFileDto {
    @ApiProperty()
    @IsOptional()
    @MinLength(10)
    generalFilesPath: string;

    @ApiProperty()
    @IsOptional()
    @MinLength(10)
    imagesPath: string;
}

