import { Injectable } from '@nestjs/common';
import { unlinkSync as removeFile, rename, existsSync } from 'fs';
import { extname as getExtName, join } from 'path';
import { promisify } from 'util'
import * as Sharp from 'sharp'
import * as gifResize from '@gumlet/gif-resize'
import { readFile, writeFile } from 'fs'


import { ImagesSize, FileType, DocumentPath } from '../../../interface';

import { fileHelper } from '@helpers';
import { ErrorType } from '../../../common/enum/error-type.enum';



const readFilePromise = promisify(readFile);
const saveFile = promisify(writeFile);
const moveFile = promisify(rename);

const imageQuality = 80;
const Heights = [128, 256, 512, 1024];

enum sizeImage {
    s128 = "thumb",
    s256 = "low",
    s512 = "medium",
    s1024 = "original"
};

@Injectable()
export class FilesService {

    /**
     * This function save all general files
     * @param files 
     * @param pathFile 
     * @returns { Promise<DocumentPath[]>}
     */
    async saveGeneralFiles(
        files: FileType[],
        pathFile: string
    ): Promise<DocumentPath[]> {

        const documents = [];
        try {
            for (const document of files) {
                const filename = fileHelper.generateFileName(document);
                const oldPath = document.path;

                const { path: publicPath, newDirPath } = await fileHelper.generateDirForGeneralFile(pathFile);
                const newPath = `${newDirPath}/${filename}`;

                await moveFile(oldPath, newPath);

                documents.push({
                    url: `${publicPath}/${filename}`
                });
            }

            return documents
        } catch (error) {
            console.log("files", { error });
            throw { error, savedDocuments: documents, errorType: ErrorType.SavingGeneralFiles };
        }

    }

    /**
     * This function save all images
     * @param images 
     * @param path 
     * @returns { Promise<ImagesSize[]> }
     */
    async saveImages(images: FileType[], path: string): Promise<ImagesSize[]> {

        const imagesUploaded = [];

        try {
            for await (const imageFile of images) {
                const data = await this.createNewImage(imageFile, path);
                imagesUploaded.push(data);
            }

            return imagesUploaded

        } catch (error) {
            throw { error, errorType: ErrorType.SavingImages, savedImages: imagesUploaded }
        }

    }

    /**
     * This function create 4 differente images with a resize and new quality 
     * @param imageFile 
     * @param pathFile 
     * @returns { Promise<ImagesSize> }
     */
    private async createNewImage(imageFile: FileType, pathFile: string): Promise<ImagesSize> {

        const urlImagen: ImagesSize = { thumb: '', low: '', medium: '', original: '' };

        const imageSharp = await Sharp(imageFile.path);
        const fileName = fileHelper.generateFileName(imageFile);
        const extname = getExtName(fileName);

        const imagesSaved = [];

        const { path: publicPath, newDirPath } = await fileHelper.generateDirForGeneralFile(pathFile);

        try {
            for (const height of Heights) {
                const newNameImage = `${height}x${height}-${fileName}`;
                const saveIn = `${newDirPath}/${newNameImage}`;

                try {
                    if (extname === '.gif') {
                        const buf = await readFilePromise(imageFile.path);
                        const data = await gifResize({
                            height,
                            width: height
                        })(buf)

                        await saveFile(saveIn, data);

                    } else {
                        let imageClone = await imageSharp.clone();

                        if (extname === '.svg') {
                            imageClone = await imageClone
                                .resize({ height });

                        } else {
                            imageClone = await imageClone
                                .resize({ height })
                                .jpeg({ quality: imageQuality })
                                .toFormat('jpeg');
                        }

                        await imageClone.toFile(saveIn);

                    }

                    urlImagen[sizeImage[`s${height}`]] = `${publicPath}/${newNameImage}`;

                } catch (error) {
                    console.log("resize image", { error });

                    throw error;
                }
            }

            return urlImagen;
        } catch (error) {
            imagesSaved.forEach(img => {
                removeFile(img);
            });
            console.log("resize image2", { error });

            throw error
        }
    }


    /**
     * This function delete all temp files
     * @param files 
     */
    async deleteTempFiles(files: FileType[]) {
        for (const file of files) {

            const isValid = await existsSync(file.path);
            if (isValid) removeFile(file.path)
        }
    }

    /**
     * This function delete all uploaded images
     * @param images 
     */
    deleteImages(images: ImagesSize[]) {
        const mainPath = fileHelper.getGeneralPath();

        for (const image of images) {
            try {
                removeFile(join(mainPath, image.thumb))
                removeFile(join(mainPath, image.low))
                removeFile(join(mainPath, image.medium))
                removeFile(join(mainPath, image.original))
            } catch { }

        }
    }

    /**
     * This funciton delete all uploaded general files 
     * @param documents 
     */
    deleteGeneralFiles(documents: DocumentPath[]) {
        const mainPath = fileHelper.getGeneralPath();

        for (const document of documents) {
            try {
                removeFile(join(mainPath, document.url))
            } catch { }
        }
    }





}
