import { ErrorType } from '@common/enum';
import { Injectable, BadRequestException } from '@nestjs/common';
import { allowedTokensEnum } from '../../../config/allowed-token.config'

@Injectable()
export class TokenUploadFileService {


    /**
     * This function validate if the token is allow or not
     * @param token 
     * @returns {boolean }
     */
    verifyUploadToken(token: any): boolean {
        const allowed = Object.values(allowedTokensEnum);

        if (!allowed.includes(token)) {
            throw new BadRequestException({
                errorType: ErrorType.InvalidToken,
                message: "Token invalid!"
            })
        }
        return true;
    }
}
