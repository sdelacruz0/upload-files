import {
    Controller,
    Post,
    UploadedFiles,
    UseInterceptors,
    Body,
    BadRequestException,
    InternalServerErrorException,
    UseGuards
} from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import { fileHelper } from '@helpers';

import { FilesService } from './services';
import { ImagesSize, DocumentPath, errorUploadingFiles } from '../../interface';
import { ErrorType } from '../../common/enum';
import { SaveFileDto } from './dtos/save-file.dto';
import { TokenUploadFileGuard } from './guards/token-upload.guard';

@Controller('files')
export class FilesController {

    constructor(private filesService: FilesService) { }

    @Post('upload')
    @UseGuards(TokenUploadFileGuard)
    @UseInterceptors(FilesInterceptor('files', 20))
    async uploadFiles(@UploadedFiles() files: Array<Express.Multer.File>, @Body() body: SaveFileDto) {

        if (!files) {
            throw new BadRequestException({
                errorType: ErrorType.NotFilesSend,
                message: "Need to send some files!"
            })
        } else {
            const values = Object.values(body)
            if (!values.length) {
                this.filesService.deleteTempFiles(files)

                throw new BadRequestException({
                    errorType: ErrorType.NotBodyParamsInclude,
                    message: "Need to send generalFilesPath or imagesPath"
                })
            }
        }


        const { generalFilesPath, imagesPath } = body

        const documentsUploaded: DocumentPath[] = [];
        const imagesUploaded: ImagesSize[] = [];


        const { images, generalFiles } = await fileHelper.separateFileByType(files);

        try {
            if (generalFiles?.length) {
                const data = await this.filesService
                    .saveGeneralFiles(generalFiles, generalFilesPath);

                documentsUploaded.push(...data);
            }


            if (images?.length) {
                const data = await this.filesService.saveImages(images, imagesPath);
                imagesUploaded.push(...data);
            }


            return { documentsUploaded, imagesUploaded };
        } catch (error) {
            const newError: errorUploadingFiles = error

            if (newError.errorType === ErrorType.SavingImages) {
                documentsUploaded?.length && this.filesService.deleteGeneralFiles(documentsUploaded)
                newError?.savedImages && this.filesService.deleteImages(newError.savedImages)
            } else {
                newError?.savedDocuments && this.filesService.deleteGeneralFiles(newError.savedDocuments)
            }


            throw new InternalServerErrorException({
                errorType: ErrorType.CouldnotSaveFiles,
                message: "Couldn't save files"
            })
        } finally {
            this.filesService.deleteTempFiles(files)
        }
    }

}
