import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { ExtractJwt } from 'passport-jwt';

import { TokenGetFileService } from '../services';


@Injectable()
export class TokenGetFileGuard implements CanActivate {
  constructor(
    private tokenService: TokenGetFileService
  ) { }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const token = ExtractJwt.fromAuthHeaderAsBearerToken()(
      context.switchToHttp().getRequest(),
    )

    try {
      this.tokenService.verifyGetToken(token)
      return true;
    } catch (error) {
      return false;
    }
  }
}
