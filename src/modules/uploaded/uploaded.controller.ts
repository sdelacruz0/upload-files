import { Controller, Get, InternalServerErrorException, Req, Res, UseGuards } from '@nestjs/common';
import { TokenGetFileGuard } from './guards/token-file.guard';
import { Request, Response } from 'express';
import { UploadedService } from './services';
import { ErrorType } from '../../common/enum';


@Controller('uploaded')
export class UploadedController {
    constructor(private uploadedService: UploadedService) { }

    @Get("*")
    @UseGuards(TokenGetFileGuard)
    async getFile(@Req() req: Request, @Res() res: Response) {

        const pathFile: string = req.params[0];
        this.uploadedService.getFile(pathFile)
            .then(({ pathFile, type }) => {
                res.header({ 'Content-Type': type })
                res.sendFile(pathFile)
            }).catch((error) => {
                throw new InternalServerErrorException({
                    errorType: ErrorType.CouldnotGetFile,
                    message: "couldn't load the file!"
                });
            });
    }
}
