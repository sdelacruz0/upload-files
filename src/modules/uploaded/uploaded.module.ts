import { Module } from '@nestjs/common';
import { UploadedController } from './uploaded.controller';
import { TokenGetFileService, UploadedService } from './services';

@Module({
    controllers: [UploadedController],
    providers: [UploadedService, TokenGetFileService]
})
export class UploadedModule {

}
