import { Injectable } from '@nestjs/common';
import { resolve } from 'path';
import * as mime from 'mime';
import { fileHelper } from '@helpers';


@Injectable()
export class UploadedService {

    /**
     * This function get the file by its path
     * @param url 
     * @returns { Promise<{ pathFile: string; type: string; }> }
     */
    async getFile(url: string): Promise<{ pathFile: string; type: string; }> {
        const mimeType: any = mime;

        const pathFile = resolve(fileHelper.getGeneralPath(), url);
        const type = await mimeType.getType(pathFile);

        return { pathFile, type };
    }

}