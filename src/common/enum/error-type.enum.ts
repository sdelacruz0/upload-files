export enum ErrorType {
    InvalidPath = "INVALID_PATH",
    CouldnotGetFile = "COULD_NOT_GET_FILE",
    FileNotFound = "FILE_NOT_FOUND",
    InvalidToken = "INVALID_TOKEN",
    SavingGeneralFiles = "SAVING_GENERAL_FILES",
    SavingImages = "SAVING_IMAGES",
    CouldnotSaveFiles = "COULD_NOT_SAVE_FILES",
    NotFilesSend = "NOT_FILES_SEND",
    NotBodyParamsInclude = "NOT_BODY_PARAMS_INCLUDES"
}