import { Injectable, NestMiddleware, NotFoundException } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { ErrorType } from '../enum/error-type.enum';
import { fileHelper } from '@helpers';

@Injectable()
export class FilePathMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: NextFunction) {

        const pathFile: string = req.params[0];

        const isValidPathFile = fileHelper.isValidPathSaved(pathFile);
        if (!isValidPathFile) {
            throw new NotFoundException({
                errorType: ErrorType.FileNotFound,
                message: "The file wasnt found!"
            })
        }
        next();
    }
}
