import { BadRequestException } from '@nestjs/common';
import { join as joinPath, extname, resolve } from 'path';
import { existsSync, mkdir } from 'fs';
import {
    AllowedMimeType, mainDirPath
} from '../config';
import { FileType, ImagesSize } from '../interface';
import { ErrorType } from '@common/enum';

export class fileHelper {

    /**
     * This function validate allowed mimetype
     * @param req 
     * @param file 
     * @param callback 
     * @returns 
     */
    static validateFilesMimeType(req: any, file: FileType, callback: (value: any, success: boolean) => any) {

        if (!AllowedMimeType.getAll().includes(file.mimetype)) {
            return callback('Only images, pdf, excel and word files are allowed!', false);
        }
        callback(null, true);
    };

    /**
     * This function modify the default name of file when is uploaded to multer
     * @param req 
     * @param file 
     * @param cb 
     */
    static editFileName(req: any, file: FileType, cb: any) {
        const fileName = file.originalname.split('.')[0].replace(' ', '_');
        const extension = extname(file.originalname);
        const randomName = new Date().getTime();

        cb(null, `${fileName}-${randomName}${extension}`);
    };

    /**
     * This function split file en 2 type: images and general files
     * @param files 
     * @returns 
     */
    static separateFileByType(files: FileType[]): Promise<{ images: FileType[], generalFiles: FileType[] }> {
        return new Promise((resolve) => {
            const images = [];
            const generalFiles = [];

            for (const file of files) {
                const mimetype = file.mimetype.toLowerCase();

                if (AllowedMimeType.getImagesMimeTypes().includes(mimetype)) {
                    images.push(file);
                } else if (AllowedMimeType.getGeneralMimeTypes().includes(mimetype)) {
                    generalFiles.push(file);
                }
            }

            resolve({
                images,
                generalFiles
            });
        });
    }

    /**
     * This function create a dir when it doesnt exist
     * @param path 
     * @returns 
     */
    static generateDirForGeneralFile(path: string): Promise<{ newDirPath: string, path: string }> {
        const newDirPath = joinPath(this.getGeneralPath(), path)

        const exisThisPath = existsSync(newDirPath);

        return new Promise((resolve, reject) => {
            if (!exisThisPath) {
                mkdir(newDirPath, { recursive: true }, (error) => {
                    if (error) return reject(error)
                    resolve({ newDirPath, path });
                })
            } else {
                resolve({ newDirPath, path });
            }
        })
    }

    /**
     * This function generate the file name 
     * @param file { FileType }
     * @returns 
     */
    static generateFileName(file: FileType): string {
        const random = (Math.random() * (1000000 - 100)) + 687641;
        const ext = extname(file.originalname);
        const fileName = `${new Date().getTime()}-${random.toFixed(0)}${ext}`;
        return fileName;
    }

    /**
     * This function get the main path of all uploaded files
     * @returns 
     */
    static getGeneralPath(): string {
        return joinPath(__dirname, mainDirPath);
    }

    /**
     * This function validate the file path exist on the storage folder
     * @param path {string}
     * @returns  boolean
     */
    static isValidPathSaved(path: string): boolean {
        try {
            const filePath = resolve(this.getGeneralPath(), path);
            const isValid = existsSync(filePath);

            return isValid;
        } catch (error) {
            throw new BadRequestException({
                errorType: ErrorType,
                message: 'File not found!'
            });
        }
    }


}