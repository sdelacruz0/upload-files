
## Description

This app is for uploading files and images with this kind of mimetypes: 

- Images
  - "image/png"
  - "image/gif"
  - "image/jpeg"
  - "image/svg+xml"

- Offices
  - "application/msword"
  - "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
  - "application/vnd.ms-excel"
  - "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  - "application/vnd.ms-excel.sheet.binary.macroEnabled.12"

- General files
  - "application/pdf"

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

